import React from 'react';
import Validation from '../Validation/Validation';
import Aux from '../../hoc/Auxiliary';
import classes from './Cockpit.scss';

const cockpit = (props) => {
    return(
        <Aux>
            <input className={classes.insert_text} type="text" value={props.inputText} onChange={props.changed} ref={props.inputRef} />
            <p>(at least 3 sings)</p>
            <p>Text length: {props.inputText.length}</p>
            <Validation textLength={props.inputText.length} />
            <p>Removed letters: {props.counter}</p>
        </Aux>
    )
};

export default cockpit;