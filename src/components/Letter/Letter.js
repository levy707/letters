import React from 'react';
import Char from './Char/Char'

const letter = (props) => props.inputText.split('').map((assignLetter, index) => {
    if (props.inputText.length >= 3) {
        return (
            <Char
                key={index}
                value={assignLetter}
                click={() => props.clicked(index)}
            />
        )
    } else {
        return null
    }
});

export default letter;