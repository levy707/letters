import React from 'react';
import classes from './Char.scss';

const char = (props) => {
    return(
        <div className={classes.char} onClick={props.click}>
            {props.value}
        </div>
    )
};

export default char;