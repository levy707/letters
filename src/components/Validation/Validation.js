import React from 'react';
import Aux from '../../hoc/Auxiliary';

const validation = (props) => {
    const message = (props.textLength >= 3) ? 'Text long enough' : 'Text to short';
    return(
        <Aux>
            <p>{message}</p>
        </Aux>
    )
};

export default validation;