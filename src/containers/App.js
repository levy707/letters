import React, {Component} from 'react';
import Cockpit from '../components/Cockpit/Cockpit';
import Letter from '../components/Letter/Letter'
import classes from  './App.scss';

class App extends Component {
    state = {
        inputText: 'text',
        removedLetterCounter: 0
    };

    textChangeHandler = (event) => {
        this.setState(
            {inputText: event.target.value}
        )
    };

    deleteLetterHandler = (index) => {
        const letter = this.state.inputText.split('');
        letter.splice(index, 1);
        this.setState((prevState, props) => {
                return {
                    inputText: letter.join(''),
                    removedLetterCounter: prevState.removedLetterCounter + 1
                }
            }
        )
    };

    componentDidMount () {
        const val = this.inputElement.value;
        this.inputElement.focus();
        this.inputElement.value = '';
        this.inputElement.value = val;
    }

    render() {
        return (
            <section className={classes.app}>
                <Cockpit
                    inputText={this.state.inputText}
                    changed={this.textChangeHandler}
                    counter={this.state.removedLetterCounter}
                    inputRef={(el) => {this.inputElement = el}}
                />
                <Letter
                    inputText={this.state.inputText}
                    clicked={this.deleteLetterHandler}
                />
            </section>
        );
    }
}

export default App;
